//
//  ArtistGridCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/14/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

final class ArtistGridCell: UICollectionViewCell, NibReusableView, ConfigurableCell {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        followersLabel.text = nil
    }
    
    
    func configured(with object: Any) -> ReusableView {
        let artist = object as! Artist
        self.nameLabel.text = artist.name
        self.followersLabel.text = "Followers: \(artist.followersCount)"
        self.photoView.kf.setImage(with: artist.imageURL, placeholder: UIImage(named: "no image"))
        return self
    }
    
    
}























