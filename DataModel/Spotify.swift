//
//  SpotifierManager.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/5/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

// MARK: Frameworks
import Foundation
import SwiftyOAuth

// MARK:- SpotifyManager Singleton
final class Spotify {
    static let shared = Spotify()
    private init() {}
    
    // MARK:- OAuth handler
    fileprivate let oauthProvider = Provider.spotify(
        clientID: "badf9c13b4604dd3b6f3335df4542100",
        clientSecret: "a889c1f8e98f4e07a93fbb56ce5d19a5")
    
    fileprivate var queuedRequests: [APIRequest] = []
    fileprivate var isFetchingOAuthToken = false {
        didSet {
            if isFetchingOAuthToken { return }
            processQueuedRequests()
        }
    }
}


typealias JSON = [String: Any]

// MARK:- Spotify error types
enum SpotifyError: Swift.Error {
    case invalidResponse
    case noData
    case invalidJSON
    case networkError(urlError: URLError?)
}

// MARK:- Search types, search elements
extension Spotify {
    enum SearchType: String {
        case artist
        case album
        case track
        case playlist
    }
    
    //	This models various items that can be requested for the given main entity
    //	For examples, see Path.artists

    enum ItemType: String {
        case albums = "albums"
        case topTracks = "top-tracks"
        case relatedArtists = "related-artists"
        case tracks = "tracks"
        case newReleases = "new-releases"
        case audioAnalysis = "audio-Analysis"
        case audioFeatures = "audio-Features"
    }
    
    enum DatePrecision: String {
        case year
        case day
    }
    
}

// MARK:- SPOTIFY WRAPPER
extension Spotify {
    enum Path {
        typealias SearchParameters = (id: String, itemType: ItemType?)
        
        // MARK:- Cases: search, artists, albums, tracks
        case search(term: String, type: SearchType)
        case artists(SearchParameters)
        case albums(SearchParameters)
        case tracks(SearchParameters)
        
        // MARK:- HTTP Method
        private var method: Spotify.Method {
            switch self {
            case .search, .artists, .albums, .tracks:
                return .GET
            }
        }

        // MARK:- HTTP Headers
        private var headers: [String: String] {
            return Spotify.commonHeaders
        }
        
        // MARK:- Path & Full URL
        private var fullURL: URL {
            var path = ""
            
            switch self {
            case .search:
                path = "search"
                
            case .artists(let id, let itemType):
                path = "artists/\(id)"
                if let itemType = itemType {
                    path = "\(path)/\(itemType)"
                }
                
            case .albums(let id, let itemType):
                path = "albums/\(id)"
                if let itemType = itemType {
                    path = "\(path)/\(itemType)"
                }

            case .tracks(let id, let itemType):
                path = "tracks/\(id)/"
                if let itemType = itemType {
                    path = "\(path)/\(itemType)"
                }
//                if let itemType = itemType {
//                    switch itemType {
//                    case .audioAnalysis, .audioFeatures:
//                        path = "\(itemType)/\(id)"
//                    default:
//                        break
//                    }
//                }
            }
            
            return baseURL.appendingPathComponent(path)
        }

        // MARK:- Parametres
        private var params: [String: String] {
            var p : [String: String] = [:]
            
            switch self {
                
            case .search(let term, let type):
                p["q"] = term
                p["type"] = type.rawValue
                
            default:
                p = [:]
                                
            }
            return p
        }
        
        
        
        // MARK: Encoded parametres
        private var encodedParams: String {
            switch self {
            // search, artist, albums, tracks requests are sent in query strings
            default:
                return queryEncoded(params: params)
            }
        }
        
        // MARK: Encode function for string query
        private func queryEncoded(params: [String: String]) -> String {
            if params.count == 0 { return "" }
            
            var arr = [String]()
            for (key, value) in params {
                let s = "\(key)=\(value)"
                arr.append(s)
            }
            return arr.joined(separator: "&")
        }
        
        // MARK: Encode function for JSON
        private func jsonEncoded(params: [String: String]) -> Data? {
            return try? JSONSerialization.data(withJSONObject: params)
        }
        
        
        // MARK:- URLRequest
        fileprivate var urlRequest: URLRequest {
            guard var components = URLComponents(url: fullURL, resolvingAgainstBaseURL: false) else { fatalError("Invalid URL") }
            
            // MARK: GET method: String query
            switch method {
            case .GET:
                components.query = queryEncoded(params: params)
            default:
                break
            }
            
            // MARK: POST method: JSON query
            guard let url = components.url else { fatalError("Invalid URL") }
            
            var r = URLRequest(url: url)
            r.httpMethod = method.rawValue
            r.allHTTPHeaderFields = headers
            
            switch method {
            case .POST:
                r.httpBody = jsonEncoded(params: params)
                break
            default:
                break
            }
            return r
        }
    }
}

extension Spotify {
    
    typealias Callback = ( JSON?, SpotifyError? ) -> Void
    
    // MARK:- Calling of call()
    func call(path: Path, callback: @escaping Callback) {
        
        // MARK: Local storage of apiRequest
        let apiRequest = (path, callback)
        
        // MARK: OAuth Authentification request
        // when token has arrived/is available
        oauth(apiRequest: apiRequest) {
            [unowned self] authRequest, localCallback in
            self.execute(urlRequest: authRequest, path: path, callback: localCallback)
        }
    }
}
        
fileprivate extension Spotify {
    
    typealias APIRequest = (path: Path, callback: Callback)
    
    // MARK:- When token arrives: processQueuedRequest()
    func processQueuedRequests() {
        for apiReq in queuedRequests {
            oauth(apiRequest: apiReq) {
                [weak self] urlRequest, callback in
                // MARK: URLSession, when token is ready and valid
                self?.execute(urlRequest: urlRequest, path: apiReq.path, callback: callback)
            }
        }
    }
    
    // MARK:- OAuth token search/check
    // Double Callback
    func oauth(apiRequest: APIRequest, completion: @escaping (URLRequest, @escaping Callback) -> Void) {
        var req = apiRequest.path.urlRequest
        
        if isFetchingOAuthToken {
            queuedRequests.append( apiRequest )
            return
        }
        
        guard let token = oauthProvider.token else {
            queuedRequests.append( apiRequest )
            fetchToken()
            return
        }
        
        if !token.isValid {
            queuedRequests.append( apiRequest )
            refreshToken()
            return
        }
        
        // MARK: Signing of exising and valid token
        switch (token.tokenType ?? .bearer) {
        case .bearer:
            req.setValue("Bearer \(token.accessToken)", forHTTPHeaderField: "Authorization")
        }
        completion(req, apiRequest.callback)
    }
    
    // MARK:- Fetch & refresh token
    func fetchToken() {
        isFetchingOAuthToken = true
        
        oauthProvider.authorize {
            [weak self] result in
            guard let `self` = self else { return }
            
            switch result {
            case .success:
                self.isFetchingOAuthToken = false
            case .failure:
                //	TO-DO: Token fetch returned an error
                break
            }
        }
    }
    
    func refreshToken() {
        fetchToken()
    }
    
    // MARK:- Network call (when token is ready and valid)
    func execute(urlRequest: URLRequest, path: Path, callback: @escaping Callback) {
        
        let task = URLSession.shared.dataTask(with: urlRequest) {
            [unowned self]
            data, urlResponse, error in
            
            // MARK: Error handling
            if let error = error as? URLError {
                //    to do: check error type
                callback(nil, SpotifyError.networkError(urlError: error))
                return
            }
            
            guard let httpURLResponse = urlResponse as? HTTPURLResponse else {
                callback(nil, SpotifyError.invalidResponse)
                return
            }
            
            if httpURLResponse.statusCode != 200 {
                switch httpURLResponse.statusCode {
                case 401:    //    Unauthorized, invalid token
                    self.oauthProvider.invalidateToken()
                    self.call(path: path, callback: callback)
                default:
                    callback(nil, SpotifyError.invalidResponse)
                }
                return
            }
            
            guard let data = data else {
                callback(nil, SpotifyError.noData)
                return
            }
            
            guard
                let obj = try? JSONSerialization.jsonObject(with: data),
                let json = obj as? JSON
                else {
                    callback(nil, SpotifyError.invalidJSON)
                    return
            }
            callback(json, nil)
        }
        task.resume()
    }
}

// MARK:- Elements for HTTPRequest
fileprivate extension Spotify {
    
    // MARK: BaseURL
    static let baseURL : URL = {
        guard let url = URL(string: "https://api.spotify.com/v1/") else { fatalError("Can't create base URL!") }
        return url
    }()

    // MARK: HTTP Headers
    static let commonHeaders: [String: String] = {
        return [
            "User-Agent": "Spotifier/1.0",
            "Accept": "application/json",
            "Accept-Charset": "utf-8",
            "Accept-Encoding": "gzip, deflate"
        ]
    }()
    
    // MARK: HTTP Methods
    enum Method: String {
        case GET, POST, PUT, DELETE, HEAD
    }
}








































