//
//  DataManager.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/5/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

// MARK: Frameworks
import Foundation
import RTCoreDataStack
import Marshal
import CoreData

// MARK:- Data error types
enum DataError: Error {
    case apiError(SpotifyError)
    case invalidJSON
    case coreDataCreateFailed
    case coreDataSaveFailed
    case jsonError(MarshalError)
}

// MARK:- Data model
final class DataManager {
    
    let spotifyManager: Spotify
    let coreDataStack: RTCoreDataStack
    
    init(spotifyManager: Spotify, coreDataStack: RTCoreDataStack) {
        self.spotifyManager = spotifyManager
        self.coreDataStack = coreDataStack
    }
}

// MARK:- API wrappers, func search()
extension DataManager {
    
    typealias Callback = (Int, DataError?) -> Void
    // MARK: Post Nofifications
    // Make API call to preload data, post notification to inform rest of app
    func preload() {
        // Recommended way of notification
        let name = Notify.dataPreloadCompleted.name
        let userInfo: [String: Any]? = nil
        let notification = Notification(name: name, object: self, userInfo: userInfo)
        NotificationCenter.default.post(notification)
    }
    
    
    func search(for term: String, type: Spotify.SearchType, callback: @escaping Callback) {
        // MARK: Path definition
        let path = Spotify.Path.search(term: term, type: type)
        // MARK: Implementation of call()
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            // MARK: CoreData update results
            switch type {
            case .artist:
                self.updateData(json: json, error: error, Type: Artist(), key: "artists", innerKey: "items", callback: callback)
            case .album:
                self.updateData(json: json, error: error, Type: Album(), key: "albums", innerKey: "items", callback: callback)
            case .track:
                self.updateData(json: json, error: error, Type: Track(), key: "tracks", innerKey: "items", callback: callback)
            default:
                break
            }
        }
    }
    
    
    func searchAlbums(for id: String, itemType: Spotify.ItemType, callback: @escaping Callback) {
        
        let path = Spotify.Path.artists((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            switch itemType {
            case .albums:
                self.loadObjects(json: json, error: error, Type: Album(), id: id, innerKey: "items", callback: callback)
            default:
                break
            }
        }
    }
    
    
    func searchTracks(for id: String, itemType: Spotify.ItemType, callback: @escaping Callback) {
        
        let path = Spotify.Path.albums((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            self.loadObjects(json: json, error: error, Type: Track(), id: id, innerKey: "items", callback: callback)
        }
    }
    
    
    func searchTrackInfo(for id: String, itemType: Spotify.ItemType?, callback: @escaping Callback) {
        
        let path = Spotify.Path.tracks((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            self.loadObjects(json: json, error: error, Type: Album(), id: id, innerKey: "items", callback: callback)
        }
    }
}


extension DataManager {
    
    func updateData<T>(json: JSON?, error: SpotifyError?, Type: T, key: String, innerKey: String, callback: @escaping Callback) {
        // MARK: Error handling
        if let error = error {
            callback(0, DataError.apiError(error))
            return
        }
        
        guard let json = json else {
            callback(0, DataError.invalidJSON)
            return
        }
        
        // MARK: CoreData notification handling
        let moc = self.coreDataStack.importerContext()
        var count: Int = 0
        
        guard let result = json[key] as? JSON else { return }
        
        do {
            switch Type {
                
            case is Artist:
                let objects: [Artist] = try result.value(for: innerKey, inContext: moc)
                let ids: [String] = objects.flatMap({$0.artistId})
                let predicate = NSPredicate(format: "%K IN %@", Artist.Attributes.artistId, ids)
                let existingObjects = Artist.fetch(in: moc, includePending: false, predicate: predicate)
                let existingIds = existingObjects.flatMap({$0.artistId})
                var updatedObjects = objects.filter({ existingIds.contains($0.artistId) })
                
                for io in updatedObjects {
                    guard let eo = existingObjects.filter({ $0.artistId == io.artistId }).first else { continue }
                    eo.name = io.name
                    eo.artistId = io.artistId
                    eo.albums = io.albums
                    eo.csvGenres = io.csvGenres
                    eo.followersCount = io.followersCount
                    eo.imageLink = io.imageLink
                    eo.popularity = io.popularity
                    eo.spotifyURI = io.spotifyURI
                }
                
                for mo in updatedObjects {
                    moc.delete(mo)
                }
                updatedObjects.removeAll()
                count = objects.count + existingObjects.count
                
                
            case is Album:
                let objects: [Album] = try result.value(for: innerKey, inContext: moc)
                let attribute = Album.Attributes.albumId
                let ids: [String] = objects.flatMap({$0.albumId})
                let predicate = NSPredicate(format: "%K IN %@", attribute, ids)
                let existingObjects = Album.fetch(in: moc, includePending: false, predicate: predicate)
                let existingIds = existingObjects.flatMap({$0.albumId})
                var updatedObjects = objects.filter({ existingIds.contains($0.albumId) })
                
                for io in updatedObjects {
                    guard let eo = existingObjects.filter({ $0.albumId == io.albumId }).first else { continue }
                    eo.name = io.name
                    eo.artistNames = io.artistNames
                    eo.artistIds = io.artistIds
                    eo.artists = io.artists
                    eo.csvAvailableMarkets = io.csvAvailableMarkets
                    eo.dateReleased = io.dateReleased
                    eo.dateReleasedPrecision = io.dateReleasedPrecision
                    eo.imageLink = io.imageLink
                    eo.labelName = io.labelName
                }
                
                for mo in updatedObjects {
                    moc.delete(mo)
                }
                updatedObjects.removeAll()
                count = objects.count + existingObjects.count
                
            default:
                let objects: [Track] = try result.value(for: innerKey, inContext: moc)
                let attribute = Track.Attributes.trackId
                let ids: [String] = objects.flatMap({$0.trackId})
                let predicate = NSPredicate(format: "%K IN %@", attribute, ids)
                let existingObjects = Track.fetch(in: moc, includePending: false, predicate: predicate)
                let existingIds = existingObjects.flatMap({$0.trackId})
                var updatedObjects = objects.filter({ existingIds.contains($0.trackId) })
                
                for io in updatedObjects {
                    guard let eo = existingObjects.filter({ $0.trackId == io.trackId }).first else { continue }
                    eo.name = io.name
                    eo.albumId = io.albumId
                    eo.artistNames = io.artistNames
                    eo.artistImage = io.artistImage
                    eo.artistIds = io.artistIds
                    eo.imageLink = io.imageLink
                    eo.previewLink = io.previewLink
                    eo.popularity = io.popularity
                    eo.discNumber = io.discNumber
                    eo.durationMilliseconds = io.durationMilliseconds
                    eo.csvAvailableMarkets = io.csvAvailableMarkets
                    eo.isExplicit = io.isExplicit
                }
                
                for mo in updatedObjects {
                    moc.delete(mo)
                }
                updatedObjects.removeAll()
                count = objects.count + existingObjects.count
            }
            
        } catch let jsonError {
            callback(0, DataError.jsonError(jsonError as! MarshalError))
            return
        }
        
        do {
            if moc.hasChanges {
                try moc.save()
            }
            callback(count, nil)
            
        } catch let error {
            print("Context Save failed due to: \(error)")
            callback(0, DataError.coreDataSaveFailed)
        }
    }
}


extension DataManager {
    
    func loadObjects<T>(json: JSON?, error: SpotifyError?, Type: T, id: String, innerKey: String, callback: @escaping Callback) {
        
        if let error = error {
            callback(0, DataError.apiError(error))
            return
        }
        
        guard let json = json else {
            callback(0, DataError.invalidJSON)
            return
        }
        
        let moc = self.coreDataStack.importerContext()
        var count: Int = 0
        
        do {
            switch Type {
                
            case is Album:
                var objects: [Album] = try json.value(for: innerKey, inContext: moc)
                let ids: [String] = objects.flatMap({$0.albumId})
                let predicate = NSPredicate(format: "%K IN %@", Album.Attributes.albumId, ids)
                let existingObjects = Album.fetch(in: moc, includePending: false, predicate: predicate)
                let existingIds = existingObjects.flatMap({$0.albumId})
                var updatedObjects = objects.filter({ existingIds.contains($0.albumId) })
                objects = objects.filter({ !updatedObjects.contains($0) })
                
                for io in updatedObjects {
                    guard let eo = existingObjects.filter({ $0.albumId == io.albumId }).first else { continue }
                    eo.name = io.name
                    eo.artistNames = io.artistNames
                    eo.artistId = id
                    eo.artistIds = io.artistIds
                    eo.artists = io.artists
                    eo.csvAvailableMarkets = io.csvAvailableMarkets
                    eo.dateReleased = io.dateReleased
                    eo.dateReleasedPrecision = io.dateReleasedPrecision
                    eo.imageLink = io.imageLink
                    eo.labelName = io.labelName
                }
                
                for mo in updatedObjects {
                    moc.delete(mo)
                }
                updatedObjects.removeAll()
                count = objects.count + existingObjects.count
                
                
            default:
                var objects: [Track] = try json.value(for: innerKey, inContext: moc)
                let ids: [String] = objects.flatMap({$0.trackId})
                let predicate = NSPredicate(format: "%K IN %@", Track.Attributes.trackId, ids)
                let existingObjects = Track.fetch(in: moc, includePending: false, predicate: predicate)
                let existingIds = existingObjects.flatMap({$0.trackId})
                var updatedObjects = objects.filter({ existingIds.contains($0.trackId) })
                objects = objects.filter({ !updatedObjects.contains($0) })
                
                for io in updatedObjects {
                    guard let eo = existingObjects.filter({ $0.trackId == io.trackId }).first else { continue }
                    eo.name = io.name
                    eo.album = io.album
                    eo.albumId = id
                    eo.artistNames = io.artistNames
                    eo.artistImage = io.artistImage
                    eo.artistIds = io.artistIds
                    eo.imageLink = io.imageLink
                    eo.previewLink = io.previewLink
                    eo.popularity = io.popularity
                    eo.discNumber = io.discNumber
                    eo.durationMilliseconds = io.durationMilliseconds
                    eo.csvAvailableMarkets = io.csvAvailableMarkets
                    eo.isExplicit = io.isExplicit
                }
                
                for mo in updatedObjects {
                    moc.delete(mo)
                }
                updatedObjects.removeAll()
                count = objects.count + existingObjects.count
            }
            
        } catch let jsonError {
            callback(0, DataError.jsonError(jsonError as! MarshalError))
            return
        }
        
        do {
            if moc.hasChanges {
                try moc.save()
            }
            callback(count, nil)
            
        } catch let error {
            print("Context Save failed due to: \(error)")
            callback(0, DataError.coreDataSaveFailed)
        }
    }
}
