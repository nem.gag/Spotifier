//
//  SearchCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/22/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

class SearchCell: UICollectionViewCell, NibReusableView, ConfigurableCell {
    
    @IBOutlet fileprivate weak var photoView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var popularityLabel: UILabel!
    @IBOutlet fileprivate weak var followersLabel: UILabel!
    @IBOutlet fileprivate weak var albumName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    fileprivate func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        popularityLabel.text = nil
        followersLabel.text = nil
        albumName.text = nil
    }
    
    func configured(with object: Any) -> ReusableView {
        
        if let artist = object as? Artist {
            self.nameLabel.text = artist.name
            self.popularityLabel.text = (artist.popularity > 0) ? "Popularity: \(artist.popularity)" : "Not popular"
            self.followersLabel.text = (artist.followersCount > 0) ? "\(artist.followersCount) followers" : "No followers"
            self.photoView.kf.setImage(with: artist.imageURL, placeholder: UIImage(named: "no image"))
        } else if let album = object as? Album {
            nameLabel.text = album.name
            popularityLabel.text = album.artistNames ?? "Unknown artist"
            self.photoView.kf.setImage(with: album.imageURL, placeholder: UIImage(named: "no album"))
        } else if let track = object as? Track {
            self.nameLabel.text = track.name
            self.albumName.text = track.artistNames
            self.photoView.image = UIImage(named: "play song 2")
        }
        
        return self
    }
}


