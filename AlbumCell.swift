//
//  AlbumCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/4/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

final class AlbumCell: UICollectionViewCell, NibReusableView, ConfigurableCell {
    
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    func cleanup() {
        nameLabel.text = nil
        photoView.image = nil
    }
    
    
    func configured(with object: Any) -> ReusableView {
        let album = object as! Album
        self.nameLabel.text = album.name
        self.photoView.kf.setImage(with: album.imageURL, placeholder: UIImage(named: "no album"))
        return self
    }
}

