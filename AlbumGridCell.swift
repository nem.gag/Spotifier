//
//  AlbumGridCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/28/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation
import Kingfisher

final class AlbumGridCell: UICollectionViewCell, NibReusableView, ConfigurableCell {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    fileprivate func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        artistLabel.text = nil
    }
    
    func configured(with object: Any) -> ReusableView {
        let album = object as! Album
        self.nameLabel.text = album.name
        self.artistLabel.text = album.artistNames
        self.photoView.kf.setImage(with: album.imageURL, placeholder: UIImage(named: "no album"))
        return self
    }
}























