//
//  ConfigurableCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 8/21/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
    
    func configured(with object: Any) -> ReusableView
}
