//
//  CollectionView-FRC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 8/9/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

extension UICollectionViewDataSource {
    
    func fetchedNumberOfSections<T>(with fetchedResultsController: NSFetchedResultsController<T>?) -> Int {
        guard let frc = fetchedResultsController else { return 0 }
        guard let sections = frc.sections else { return 0 }
        return sections.count
    }
    
    func fetchedNumberOfItems<T>(in section: Int, with fetchedResultsController: NSFetchedResultsController<T>?) -> Int {
        guard let frc = fetchedResultsController else { return 0 }
        guard let sections = frc.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func fetchedCellForItem<T, U: NibReusableView & ConfigurableCell>
        (at indexPath: IndexPath,
         in collectionView: UICollectionView,
         with fetchedResultsController: NSFetchedResultsController<T>?) -> U {
        
        guard let frc = fetchedResultsController else { fatalError("Should never happen!") }
        let obj = frc.object(at: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: U.reuseIdentifier, for: indexPath) as! ConfigurableCell
        return cell.configured(with: obj) as! U
    }
}


extension NSFetchedResultsControllerDelegate {
    
    func openSelectedItem<T: StoryboardLoadableHelper & NeedsDependency & FetchableObject, U>
        (at index: Int,
         in controller: T,
         from currentViewController: UIViewController,
         dependency: Dependency?,
         with fetchedResultsController: NSFetchedResultsController<U>?) {
                
        var vc = controller
        vc.dependency = dependency
        vc.fetchedObject = fetchedResultsController?.fetchedObjects![index] as? NSManagedObject
        currentViewController.show(vc as! UIViewController, sender: self)
    }
}









