//
//  ArtistGreedController.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/12/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController, StoryboardLoadable, NeedsDependency {
    
    var dependency: Dependency? {
        didSet {
            if let vc = searchController {
                vc.dependency = dependency
            }
            if !self.isViewLoaded { return }
            prepareDataSource()
        }
    }
    
    var dataManager: DataManager? { return dependency?.dataManager }
    var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
//    let dataSource = DataSource()
    
    var frcArtist: NSFetchedResultsController<Artist>?
    var frcAlbum: NSFetchedResultsController<Album>?
    var frcTrack: NSFetchedResultsController<Track>?
    
    fileprivate var searchController: SearchController?
    
    @IBOutlet fileprivate weak var collectionViewArtist: UICollectionView!
    @IBOutlet fileprivate weak var collectionViewAlbum: UICollectionView!
    @IBOutlet fileprivate weak var collectionViewTrack: UICollectionView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataSource()
        setupNotificationHandlers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


fileprivate extension MainVC {
    
    func setupNotificationHandlers() {
        NotificationCenter.default.addObserver(
            forName: DataManager.Notify.dataPreloadCompleted.name,
            object: nil,
            queue: OperationQueue.main) {
                [weak self] notification in
                guard let `self` = self else { return }
                self.prepareDataSource()
        }
    }
}


// MARK:- prepareDataSource()
extension MainVC {
    
    func prepareDataSource() {
        prepareArtistDataSource()
        prepareAlbumDataSource()
        prepareTrackDataSource()
    }
}


extension MainVC {
    
    fileprivate func prepareArtistDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Artist> = Artist.fetchRequest()
        
        let sort = NSSortDescriptor(key: Artist.Attributes.popularity, ascending: false)
        let sort2 = NSSortDescriptor(key: Artist.Attributes.followersCount, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        frcArtist = NSFetchedResultsController(fetchRequest: fr,
                                               managedObjectContext: moc,
                                               sectionNameKeyPath: nil,
                                               cacheName: nil)
        
        
//        frcArtist = dataSource.prepareArtistDataSource()
        
        frcArtist?.delegate = self
        
        if ((try? frcArtist?.performFetch()) != nil) {
            collectionViewArtist.reloadData()
        }
    }
    
    fileprivate func prepareAlbumDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Album> = Album.fetchRequest()
        
        let sort = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let sort2 = NSSortDescriptor(key: Album.Attributes.albumId, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        frcAlbum = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
        
//        frcAlbum = dataSource.prepareMainVCAlbumDataSource()
        
        frcAlbum?.delegate = self
        
        if ((try? frcAlbum?.performFetch()) != nil) {
            collectionViewAlbum.reloadData()
        }
    }
    
    fileprivate func prepareTrackDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Track> = Track.fetchRequest()
        
        let sort = NSSortDescriptor(key: Track.Attributes.popularity, ascending: false)
        fr.sortDescriptors = [sort]
        
        frcTrack = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
        
//        frcTrack = dataSource.prepareMainVCTrackDataSource()
        
        frcTrack?.delegate = self
        
        if ((try? frcTrack?.performFetch()) != nil) {
            collectionViewTrack.reloadData()
        }
    }
}


// MARK:- FRC Delegate
extension MainVC: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        if controller == frcArtist {
            collectionViewArtist.reloadData()
        } else if controller == frcAlbum {
            collectionViewAlbum.reloadData()
        } else {
            collectionViewTrack.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case collectionViewArtist:
            openSelectedItem(at: indexPath.row, in: ArtistVC.instantiate(), from: self, dependency: dependency, with: frcArtist)
        case collectionViewAlbum:
            openSelectedItem(at: indexPath.row, in: AlbumVC.instantiate(), from: self, dependency: dependency, with: frcAlbum)
        default:
            openSelectedItem(at: indexPath.row, in: TrackVC.instantiate(), from: self, dependency: dependency, with: frcTrack)
        }
    }
}

// MARK:- UICollectionView DataSource
extension MainVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        switch collectionView {
        case collectionViewArtist:
            return fetchedNumberOfSections(with: frcArtist)
        case collectionViewAlbum:
            return fetchedNumberOfSections(with: frcAlbum)
        default:
            return fetchedNumberOfSections(with: frcTrack)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case collectionViewArtist:
            return fetchedNumberOfItems(in: section, with: frcArtist)
        case collectionViewAlbum:
            return fetchedNumberOfItems(in: section, with: frcAlbum)
        default:
            return fetchedNumberOfItems(in: section, with: frcTrack)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case collectionViewArtist:
            return fetchedCellForItem(at: indexPath, in: collectionView, with: frcArtist) as ArtistGridCell
        case collectionViewAlbum:
            return fetchedCellForItem(at: indexPath, in: collectionView, with: frcAlbum) as AlbumGridCell
        default:
            return fetchedCellForItem(at: indexPath, in: collectionView, with: frcTrack) as TrackGridCell
        }
    }
}


// MARK:- CollectionView Layout
extension MainVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case collectionViewArtist:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 2, layout: collectionViewLayout)
        case collectionViewAlbum:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 1, layout: collectionViewLayout)
        default:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 1, layout: collectionViewLayout)
        }
    }
}


// MARK:- Action activateSearch(), func openSearch()
extension MainVC {
    @IBAction func activateSearch(_ sender: UIBarButtonItem) {
        openSearch()
    }
    
    func openSearch() {
        let vc = SearchController.initial()
        vc.dependency = dependency
        show(vc, sender: self)
        searchController = vc
    }
    
    func initiateAlbumSearch(artistID: String?) {
        guard let dataManager = dataManager, let id = artistID else { return }
        dataManager.searchAlbums(for: id, itemType: .albums) {
            count, dataError in
            print(count)
        }
    }
}










