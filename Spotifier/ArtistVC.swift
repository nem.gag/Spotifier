//
//  ArtistVC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/29/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher


class ArtistVC: UIViewController, StoryboardLoadableHelper, NeedsDependency, FetchableObject {
    
    typealias T = ArtistVC
    static func instantiate() -> ArtistVC {
        return ArtistVC.instantiate(fromStoryboardNamed: "Main")
    }
    
    var dependency: Dependency? {
        didSet {
            if let vc = searchController {
                vc.dependency = self.dependency
            }
            if !self.isViewLoaded { return }
            prepareAlbumDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    
    var fetchedObject: NSManagedObject? {
        didSet {
            artist = fetchedObject as? Artist
        }
    }
    
    
    var artist: Artist? {
        didSet {
            initiateAlbumSearch(artistID: artist!.artistId)
        }
    }
    
    fileprivate var searchController: SearchController?
    
    fileprivate var frc: NSFetchedResultsController<Album>?
    
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var artistLabel: UILabel!
    @IBOutlet fileprivate weak var popularityLabel: UILabel!
    @IBOutlet fileprivate weak var followersLabel: UILabel!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}


extension ArtistVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureArtist()
        prepareAlbumDataSource()
    }
}


extension ArtistVC {
    
    @IBAction func activateSearch(_ sender: UIBarButtonItem) {
        openSearch()
    }
    
    func openSearch() {
        let vc = SearchController.initial()
        vc.dependency = dependency
        show(vc, sender: self)
        searchController = vc
    }

    
    func initiateAlbumSearch(artistID: String?) {
        guard let dataManager = dataManager, let id = artistID else { return }
        dataManager.searchAlbums(for: id, itemType: .albums) {
            count, dataError in
            print(count)
        }
    }
    
    func configureArtist() {
        artistLabel.text = artist?.name ?? "Unknown artist"
        popularityLabel.text = (artist?.popularity != nil) ? "Popularity: \((artist?.popularity)!)" : ""
        followersLabel.text = (artist?.followersCount != nil) ? "Popularity: \((artist?.followersCount)!)" : ""
        imageView.kf.setImage(with: artist?.imageURL, placeholder: UIImage(named: "EmptyImage"))
    }
    
    func prepareAlbumDataSource() {
        
        initiateAlbumSearch(artistID: artist?.artistId)

        guard let moc = moc, let id = artist?.artistId else {
            frc = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let predicate = NSPredicate(format: "%K == %@", Album.Attributes.artistId, id)
        
        frc = Album.fetchedResultsController(in: moc, predicate: predicate, sortedWith: [sort])
        frc?.delegate = self
        
        guard ((try? frc?.performFetch()) != nil) else { return }
        collectionView.reloadData()
    }
}


extension ArtistVC: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openSelectedItem(at: indexPath.row, in: AlbumVC.instantiate(), from: self, dependency: dependency, with: frc)
    }
}

extension ArtistVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedNumberOfSections(with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedNumberOfItems(in: section, with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return fetchedCellForItem(at: indexPath, in: collectionView, with: frc) as AlbumCell
    }
}

extension ArtistVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 2, layout: collectionViewLayout)
    }
}







