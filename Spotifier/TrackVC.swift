//
//  TrackVC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/25/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import AVFoundation

class TrackVC: UIViewController, StoryboardLoadableHelper, NeedsDependency, FetchableObject {
    
    typealias T = TrackVC
    class func instantiate() -> T {
        return TrackVC.instantiate(fromStoryboardNamed: "Main")
    }

    var dependency: Dependency? {
        didSet {
            if let vc = searchController {
                vc.dependency = dependency
            }
            if !self.isViewLoaded { return }
//            prepareTrackDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }

    var fetchedObject: NSManagedObject? {
        didSet {
            track = fetchedObject as? Track
        }
    }

    var track: Track? {
        didSet {
            initiateTrackInfo(trackId: track?.trackId)
        }
    }
    
    fileprivate var searchController: SearchController?
    
    fileprivate var audioPlayer = AVPlayer()
    
    @IBOutlet fileprivate weak var trackLabel: UILabel!
    @IBOutlet fileprivate weak var albumLabel: UILabel!
    @IBOutlet fileprivate weak var artistLabel: UILabel!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var albumImage: UIImageView!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}

extension TrackVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTrackElements()
        downloadAndPlaySongPreview()
    }
}

extension TrackVC {
    
    @IBAction func activateSearch(_ sender: UIBarButtonItem) {
        audioPlayer.pause()
        openSearch()
    }
    
    func openSearch() {
        let vc = SearchController.initial()
        vc.dependency = dependency
        show(vc, sender: self)
        searchController = vc
    }
    
    func initiateTrackInfo(trackId: String?) {
        guard let dataManager = dataManager, let id = trackId else { return }
        
        dataManager.searchTrackInfo(for: id, itemType: nil) {
            (count, dataError) in
            print(count)
        }
    }
    
    func configureTrackElements() {
        imageView.image = UIImage(named: "song playing")
        trackLabel.text = track?.name ?? "Unknown track name"
        albumLabel.text = track?.album?.name ?? "Unknown album"
        artistLabel.text = track?.artistNames ?? "Unknown artist"
        albumImage.kf.setImage(with: track?.album?.imageURL, placeholder: UIImage(named: "EmptyImage"))
    }
    
    func downloadAndPlaySongPreview() {
        guard let link = track?.previewLink, let url = URL(string: link) else { return }
        audioPlayer = AVPlayer(url: url)
        audioPlayer.volume = 1.0
        audioPlayer.play()
    }
}











