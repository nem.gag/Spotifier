//
//  StoryboardLoadableHelper.swift
//  Spotifier
//
//  Created by Igor Stojanovic on 9/2/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

protocol StoryboardLoadableHelper : StoryboardLoadable {
    associatedtype T
    static func instantiate() -> T
}

