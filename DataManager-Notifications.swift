//
//  DataManager-Notifications.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/22/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation

// iOS SDK way:
extension Notification.Name {
    static let DataManagerDataPreloadCompleted = "DataManagerDidCompletePreloadDataNotification"
}

// recommended way:
extension DataManager {
    enum Notify: String {
        case dataPreloadCompleted = "DataManagerDidCompletePreloadDataNotification"
        
        var name: Notification.Name {
            return Notification.Name(rawValue: self.rawValue)
        }
    }
}











